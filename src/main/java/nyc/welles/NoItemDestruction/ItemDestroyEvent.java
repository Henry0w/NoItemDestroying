package nyc.welles.NoItemDestruction;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import sun.font.TrueTypeFont;

public class ItemDestroyEvent implements Listener {
    @EventHandler
    public void onItemDamaged (EntityDamageEvent entityDamageEvent){
        System.out.println("Item Damage Event called");
        if (entityDamageEvent.getEntity().getType().equals(EntityType.DROPPED_ITEM)){
            entityDamageEvent.setDamage(0);
        }
        else return;
    }
    /*@EventHandler
    public void onItemDropped(ItemSpawnEvent itemSpawnEvent){
        System.out.println("onItemDropped called");
        itemSpawnEvent.getEntity().setInvulnerable(true);
    }*/
}
